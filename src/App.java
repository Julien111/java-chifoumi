import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;


public class App {
    public static void main(String[] args) throws Exception {
        int scoreOrdi = 0;
        int scorePlayeur = 0;
        int scoreManche = 0;

        Scanner scan = new Scanner(System.in);

        System.out.println("Bienvenue dans le jeu du Chifoumi.");
        System.out.println("Cette partie va se jouer en 3 manches.");
        System.out.println("Vous allez jouer contre l'ordinateur.");

        // boucle avec le score
        while(scoreManche != 3){

            System.out.println("Choisissez entre : pierre, papier ou ciseaux ?");       
            String answerPlayeur = scan.next();
            // le choix du joueur 		
		
		    System.out.println("Vous avez choisi : " + answerPlayeur + ".");

        
            //on vérifie si il n'y a pas d'erreur
                
		    while(!answerPlayeur.equalsIgnoreCase("papier") && !answerPlayeur.equalsIgnoreCase("ciseaux") && !answerPlayeur.equalsIgnoreCase("pierre")) {
			System.out.println("Vous n'avez pas choisi parmi les 3 choix, essayer encore : ");
			answerPlayeur = scan.next();
		    }


            if(answerPlayeur.equals("papier") || answerPlayeur.equals("ciseaux") || answerPlayeur.equals("pierre")){
                // choix de l'ordinateur
                // on déclare le tableaux de valeurs possibles

                String[] choixOrdi = {"pierre", "papier", "ciseaux"};

                int randomNum = ThreadLocalRandom.current().nextInt(0, 2 + 1);
                String answerOrdi = choixOrdi[randomNum];

                if(answerPlayeur.equals(answerOrdi)){
                    System.out.println("Egalité !");
                }
                else if(answerPlayeur.equals("papier")){
                    if(answerOrdi.equals("ciseaux")){
                        System.out.println("L'ordinateur a choisi ciseaux, il gagne !");
                        scoreOrdi += 1;
                        scoreManche += 1;
                    }
                    else{
                        System.out.println("L'ordinateur a choisi pierre, vous gagnez cette manche !");
                        scorePlayeur += 1;
                        scoreManche += 1;
                    }
                }
                else if(answerPlayeur.equals("ciseaux")){
                    if(answerOrdi.equals("pierre")){
                        System.out.println("L'ordinateur a choisi pierre, il gagne !");
                        scoreOrdi += 1;
                        scoreManche += 1;
                    }
                    else{
                        System.out.println("L'ordinateur a choisi papier, vous gagnez cette manche !");
                        scorePlayeur += 1;
                        scoreManche += 1;
                    }
                }
                else{
                    if(answerPlayeur.equals("pierre")){

                        if(answerOrdi.equals("papier")){
                        System.out.println("L'ordinateur a choisi papier, il gagne !");
                        scoreOrdi += 1;
                        scoreManche += 1;
                        }
                        else{
                        System.out.println("L'ordinateur a choisi ciseaux, vous gagnez cette manche !");
                        scorePlayeur += 1;
                        scoreManche += 1;
                        }
                    }   

                }

            }
        }    
        if(scoreOrdi > scorePlayeur){
            System.out.println("L'ordinateur a gagné la partie sur le score de : " + scoreOrdi + " à " + scorePlayeur);
        }
        else{
            System.out.println("Vous avez gagné la partie sur le score de : " + scorePlayeur + " à " + scoreOrdi);
        }
    } 
}
